<?php

/**
 * @file
 * Holds the administration form for the calloutbanner module.
 */

/**
 * Form constructor for the configuration form.
 *
 * @ingroup forms
 */
function _calloutbanner_config_form($form, &$form_state) {

  global $user;

  // Get the default user format.
  $default_user_format = filter_default_format($user);

  // Include the functions.
  module_load_include('inc', 'calloutbanner', 'includes/functions');

  // Add the javascript.
  _calloutbanner_add_js();

  // Get the banner text.
  $value = variable_get('calloutbanner_text', '');

  // The banner text.
  $form['calloutbanner_text'] = array(
    '#title' => t('Callout banner text'),
    '#type' => 'text_format',
    '#description' => t('The text on the banner. (Everything but "bold, italic, underline and links" will be filtered out.)'),
    '#rows' => 5,
    '#format' => $default_user_format,
    '#default_value' => is_array($value) && array_key_exists('value', $value) ? $value['value'] : $value,
  );

  // The text color.
  $form['calloutbanner_text_color'] = array(
    '#title' => t('Text color'),
    '#type' => 'textfield',
    '#description' => t('The colour of the text.'),
    '#default_value' => variable_get('calloutbanner_text_color', '#ffffff'),
    '#attributes' => array(
      'class' => array(
        'input-0',
      ),
    ),
  );

  // The background color.
  $form['calloutbanner_link_color'] = array(
    '#title' => t('Link color'),
    '#type' => 'textfield',
    '#description' => t('The colour of the links.'),
    '#default_value' => variable_get('calloutbanner_link_color', '#ffffff'),
    '#attributes' => array(
      'class' => array(
        'input-1',
      ),
    ),
  );

  // The background color.
  $form['calloutbanner_background_color'] = array(
    '#title' => t('Background color'),
    '#type' => 'textfield',
    '#description' => t('The colour of the background.'),
    '#default_value' => variable_get('calloutbanner_background_color', '#00a5ea'),
    '#attributes' => array(
      'class' => array(
        'input-2',
      ),
    ),
  );

  // Add extra submit handler.
  $form['#submit'][] = '_calloutbanner_config_form_submit';

  // Save the settings.
  return system_settings_form($form);
}

/**
 * Form submit handler.
 *
 * @ingroup forms
 */
function _calloutbanner_config_form_submit($form, &$form_state) {

  // Include the functions.
  module_load_include('inc', 'calloutbanner', 'includes/functions');

  // Clear the frontpage cache.
  _calloutbanner_clear_frontpage_cache();
}
