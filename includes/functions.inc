<?php

/**
 * @file
 * This file holds the functions for the callout banner module.
 */

/**
 * Get the banner block content.
 *
 * @return string
 */
function calloutbanner_get_banner_content() {

  // Initialize the variables.
  $variables = array();

  // Get the banner text setting.
  $value = variable_get('calloutbanner_text', '');

  // Get the value (due to text_format).
  $banner_text = is_array($value) && array_key_exists('value', $value) ? $value['value'] : $value;

  // Set the allowed tags.
  $tags = array(
    '<strong>',
    '<em>',
    '<u>',
    '<a>',
  );

  // Make sure at least one module implements our hook.
  if (sizeof(module_implements('calloutbanner_tags_alter')) > 0) {
    // Call all modules that implement the hook, and let them make changes to $tags.
    $tags = module_invoke_all('calloutbanner_tags_alter', $tags);
  }

  // Implode the array to created the allowed tags string.
  $allowed_tags = implode(' ', $tags);

  // Strip non-allowable tags.
  $variables['banner_text'] = strip_tags($banner_text, $allowed_tags);

  // Return the themed output.
  return theme('banner_block', $variables);
}

/**
 * Add the javascript and the settings.
 */
function _calloutbanner_add_js() {

  // Get the module path.
  $module_path = drupal_get_path('module', 'calloutbanner');

  // Add farbtastic for color pickers.
  drupal_add_css('misc/farbtastic/farbtastic.css');
  drupal_add_js('misc/farbtastic/farbtastic.js');

  // Add the color picker jquery.
  drupal_add_js($module_path . '/assets/js/color-picker.js');

  // The fields which require a color-picker.
  $color_fields = _calloutbanner_get_color_fields();

  // Add settings for Drupal behaviour.
  $js_setting = array(
    'calloutbanner' => array(
      'colorPickerCount' => count($color_fields),
    ),
  );

  // Add the js.
  drupal_add_js($js_setting, 'setting');
}

/**
 * Add the inline css.
 */
function _calloutbanner_apply_css() {

  // Add the text color inline.
  $text_color = variable_get('calloutbanner_text_color', '');
  if (!empty($text_color)) {
    drupal_add_css('#callout-banner .banner-content{color:' . $text_color . ';}', 'inline');
  }

  // Add the link color inline.
  $link_color = variable_get('calloutbanner_link_color', '');
  if (!empty($link_color)) {
    drupal_add_css('#callout-banner .banner-content a{color:' . $link_color . ';border-color: ' . $link_color . '}', 'inline');
  }

  // Add the background color inline.
  $background_color = variable_get('calloutbanner_background_color', '');
  if (!empty($background_color)) {
    drupal_add_css('#callout-banner{background-color:' . $background_color . ';}', 'inline');
  }
}

/**
 * Get the color fields.
 *
 * @return array
 *   Returns an array of color fields.
 */
function _calloutbanner_get_color_fields() {

  // The fields which require a color-picker.
  return array(
    'calloutbanner_text_color',
    'calloutbanner_link_color',
    'calloutbanner_background_color',
  );
}

/**
 * Clear the frontpage cache.
 */
function _calloutbanner_clear_frontpage_cache() {
  $url = url('<front>', array('absolute' => TRUE));
  cache_clear_all($url, 'cache_page');
}
