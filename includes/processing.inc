<?php

/**
 * @file
 * The include file for the processing related functions.
 */

/**
 * Implements hook_preprocess_HOOK().
 */
function calloutbanner_preprocess_page(&$variables) {

  // Check if we're on the front page.
  if (drupal_is_front_page()) {

    // Include the functions.
    module_load_include('inc', 'calloutbanner', 'includes/functions');

    // Get the banner block content.
    $content = calloutbanner_get_banner_content();

    // Check if there is content.
    if (!empty($content)) {

      // Get the module path.
      $module_path = drupal_get_path('module', 'calloutbanner');

      // Add the css.
      drupal_add_css($module_path . '/assets/css/calloutbanner.css');

      // Add the core cookie library.
      drupal_add_library('system', 'jquery.cookie');

      // Add the jquery.
      drupal_add_js($module_path . '/assets/js/calloutbanner.js');

      // Add the css.
      _calloutbanner_apply_css();

      // Render the block content.
      print render($content);
    }
  }
}
