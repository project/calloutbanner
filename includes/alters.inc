<?php

/**
 * @file
 * This file holds the alters for the callout banner module.
 */

/**
 * Implements hook_form_BASE_FORM_ID_alter().
 */
function calloutbanner_form__calloutbanner_config_form_alter(&$form, &$form_state, $form_id) {

  // Include the functions.
  module_load_include('inc', 'calloutbanner', 'includes/functions');

  // Load script and get fields.
  $color_fields = _calloutbanner_get_color_fields();

  // Loop through the fields and append the color holder divs.
  foreach ($color_fields as $key => $field) {

    // Check if the background field exists.
    if (isset($form[$field])) {

      // Append holder for the colorpicker.
      $form[$field]['#suffix'] = '<div id="bg-color-' . $key . '"></div>';
    }
  }

  // Hide the text format.
  unset($form['calloutbanner_text'][LANGUAGE_NONE][0]['format']);
}
