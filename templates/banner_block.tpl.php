<?php

/**
 * @file
 * Template file for the callout banner.
 */
?>
<?php if (!empty($banner_text)): ?>
  <div id="callout-banner" class="hidden">
    <div class="banner-content">
    <span class="text">
      <?php print $banner_text; ?>
    </span>
      <span class="remove-banner">x</span>
    </div>
  </div>
<?php endif; ?>
