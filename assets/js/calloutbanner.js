/**
 * @File this file contains javascript required for the callout banner module.
 */
(function ($) {
  "use strict";

  Drupal.behaviors.calloutbanner = {
    attach: function (context, settings) {

      // Get the banner.
      var banner = $('#callout-banner');

      // Check if the banner exists.
      if (banner.length > 0) {

        if (!$.cookie('bannerHidden')) {

          // Remove the hidden.
          banner.removeClass('hidden');

          // Trigger on click.
          $('.remove-banner', banner).on('click', function () {

            // Remove the banner.
            banner.fadeOut(500);

            // Set the cookie.
            $.cookie('bannerHidden', true);
          });
        }
      }
    }
  };
})(jQuery);
