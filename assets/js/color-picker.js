/**
 * @file
 * Farbtastic color picker.
 */

/**
 * Show farbtastic colorpicker widget.
 */
(function ($) {
  "use strict";

  Drupal.behaviors.calloutbanner_color_picker = {
    attach: function (context, settings) {

      // Only trigger once.
      if (settings.hasOwnProperty('calloutbanner')) {

        // Check if the settings exist.
        if (settings.calloutbanner.hasOwnProperty('colorPickerCount')) {

          // Get the count.
          var count = settings.calloutbanner.colorPickerCount;

          // Loop through each item.
          for (var i = 0; i < count; i++) {

            // Check if the field exists.
            var backgroundField = $('#bg-color-' + i);

            // Check the field.
            if (backgroundField.length > 0) {

              // Fetch the input element.
              var input = backgroundField.parent().find('input.input-' + i);
              var val = input.val();

              // Small hack to make farbtastic work.
              if (val == '') {
                input.val(' ');
              }

              // Try to apply farbtastic, if this fails log it.
              try {
                // Apply farbtastic to textfield within.
                backgroundField.farbtastic(input);
              }
              catch (e) {
                console.error(e);
              }
            }
          }
        }
      }

    }
  }
})(jQuery);
