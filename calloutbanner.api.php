<?php

/**
 * @file
 * Hooks provided by the Callout Banner module.
 */

/**
 * Implements hook_change_my_entity_delete_message().
 */
function SOME_OTHER_MODULE_calloutbanner_tags_alter($tags) {
  $tags[] = '<sup>';
  return $tags;
}
